package com.groxx.freemobile;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

@SuppressLint("UnlocalizedSms")
public class MainActivity extends Activity {
  
  private TextView m_statusText;
  private ProgressBar m_progressBar;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    
    Button button = (Button) findViewById(R.id.button1);
    button.setOnClickListener(new View.OnClickListener() {
      
      @Override
      public void onClick(View v) {
        sendUpdateSms();
      }
    });
    
    m_statusText = (TextView) findViewById(R.id.text1);
    m_progressBar = (ProgressBar) findViewById(R.id.progressBar1);
    m_progressBar.setVisibility(View.INVISIBLE);
    
    IntentFilter filter = new IntentFilter();
    filter.addAction("android.provider.Telephony.SMS_RECEIVED");
    filter.setPriority(999);
    this.registerReceiver(new SmsReceiver(), filter);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.activity_main, menu);
    return true;
  }
  
  private void sendUpdateSms()
  {
    String SENT = "SMS_SENT";
    String DELIVERED = "SMS_DELIVERED";

    PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
        new Intent(SENT), 0);

    PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
        new Intent(DELIVERED), 0);

    //---when the SMS has been sent---
    registerReceiver(new BroadcastReceiver(){
        @Override
        public void onReceive(Context arg0, Intent arg1) {
          if (getResultCode() != RESULT_OK)
          {
            m_progressBar.setVisibility(View.INVISIBLE);
          }
        }
    }, new IntentFilter(SENT));

    //---when the SMS has been delivered---
    registerReceiver(new BroadcastReceiver(){
        @Override
        public void onReceive(Context arg0, Intent arg1) {
          if (getResultCode() != RESULT_OK)
          {
            m_progressBar.setVisibility(View.INVISIBLE);
          }
        }
    }, new IntentFilter(DELIVERED)); 
    
    
    
    SmsManager.getDefault().sendTextMessage("555", null, " ", sentPI, deliveredPI);
    m_progressBar.setVisibility(View.VISIBLE);
  }
  
  private void updateText(String text)
  {
    m_statusText.setText(text);
    m_progressBar.setVisibility(View.INVISIBLE);
  }
  
  private class SmsReceiver extends BroadcastReceiver
  {

    @Override
    public void onReceive(Context context, Intent intent) {
      clearAbortBroadcast();

      
      
    }

  }

}
