package com.groxx.freemobile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

public class SmsReceiver extends BroadcastReceiver
{

  @Override
  public void onReceive(Context context, Intent intent) {

    Bundle bundle = intent.getExtras();
    SmsMessage[] msgs = null;
    if (bundle != null)
    {
        //---retrieve the SMS message received---
        Object[] pdus = (Object[]) bundle.get("pdus");
        msgs = new SmsMessage[pdus.length];
        for (int i = 0; i < msgs.length; i++)
        {
            msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
            if (msgs[i].getOriginatingAddress().compareTo("555") == 0)
            {
              Toast.makeText(context, msgs[i].getMessageBody(), Toast.LENGTH_LONG).show();
              abortBroadcast();
            }
        }
    }
    
  }

}
